import telegram
from telegram import Update
from telegram import KeyboardButton
from telegram import ReplyKeyboardMarkup
from telegram import ReplyKeyboardRemove
from telegram.ext import Updater
from telegram.ext import CallbackContext
from telegram.ext import Filters
from telegram.ext import MessageHandler

button_monday = "Monday"
button_tuesday = "Tuesday"
button_wednesday = "Wednesday"
button_thursday = "Thursday"
button_friday = "Friday"
button_saturday = "Saturday"
button_help = "Help"

menu_markup = ReplyKeyboardMarkup(
    keyboard = [
        [
            KeyboardButton(text = "Menu")
        ]
    ]
)

def log(ipdate, context):
    pass

def start(update, context):
    context.bot.send_message(
        chat_id=update.effective_chat.id,
        text="I'm school bot, please talk to me!",
        reply_markup = menu_markup
        )

def button_help_handler(update: Update, context: CallbackContext):
    update.message.reply_text(
        text = "I can't help you, cause my creater so stupid",
        reply_markup = menu_markup,
        )
def button_monday_handler(update: Update, context: CallbackContext):
    update.message.reply_text(
        text = "ОБЖ\nОбществознание\nИнформатика\nФизика\nФизика",
        reply_markup = menu_markup,
        )
def button_tuesday_handler(update: Update, context: CallbackContext):
    update.message.reply_text(
        text = "Математика\nМатематика\nАнгл. Язык\nЛитература\nИстория",
        reply_markup = menu_markup,
        )
def button_wednesday_handler(update: Update, context: CallbackContext):
    update.message.reply_text(
        text = "Физкультура\nИнформатика\nИнформатика\nЛитература\nАнгл. Язык\nХимия",
        reply_markup = menu_markup,
        )
def button_thursday_handler(update: Update, context: CallbackContext):
    update.message.reply_text(
        text = "Математика\nМатематика\nФизика\nФизика\nФизкультура",
        reply_markup = menu_markup,
        )
def button_friday_handler(update: Update, context: CallbackContext):
    update.message.reply_text(
        text = "Математика\nМатематика\nОбществознание\nИнформатика\nИстория\nФизика",
        reply_markup = menu_markup,
        )
def button_saturday_handler(update: Update, context: CallbackContext):
    update.message.reply_text(
        text = "Ря/Рл\nАнгл. Язык\nРусский\nЛитература\nАстрономия",
        reply_markup = menu_markup,
        )
def message_handler(update: Update, context: CallbackContext):
    text = update.message.text
    if text == "/start":
        return start(update = update, context = context)
    elif text == button_help:
        return button_help_handler(update = update, context = context)
    elif text == button_monday:
        return button_monday_handler(update = update, context = context)
    elif text == button_tuesday:
        return button_tuesday_handler(update = update, context = context)
    elif text == button_wednesday:
        return button_wednesday_handler(update = update, context = context)
    elif text == button_thursday:
        return button_thursday_handler(update = update, context = context)
    elif text == button_friday:
        return button_friday_handler(update = update, context = context)
    elif text == button_saturday:
        return button_saturday_handler(update = update, context = context)

    reply_markup = ReplyKeyboardMarkup(
        keyboard = [
            [
                KeyboardButton(text = button_monday),
                KeyboardButton(text = button_thursday)
            ],
            [
                KeyboardButton(text = button_tuesday),
                KeyboardButton(text = button_friday)
            ],
            [
                KeyboardButton(text = button_wednesday),
                KeyboardButton(text = button_saturday)
            ],
            [
                KeyboardButton(text = button_help)
            ]
        ],
        resize_keyboard = True,
    )

    update.message.reply_text(
        text = "Hi, click on day of the week below",
        reply_markup = reply_markup,
    )

def main():
    print("Start")

    updater = Updater(
        token = "1547934825:AAF-omxPy_L-cQHSC9FvzgytdXmU3WA0vcA",
        use_context = True,
    )

    updater.dispatcher.add_handler(MessageHandler(filters = Filters.all, callback = message_handler))

    updater.start_polling()
    updater.idle()

if __name__ == "__main__":
    main()
